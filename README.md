jfinal-zbus插件

更新说明：
1）发送接受全部泛型化，更容易写出类型安全，高质量的代码。
2）支持直接发送JFinal中的Model，Record对象。
3）简化设计，不支持异步消息，只支持同步消息。
4）建议仅发送简单对象，类似List<Map<String,Model>>的复杂对象暂不支持。

使用方法：
在JFinal的Config配置文件中配置
```
//初始化zbus插件
ZbusPlugin zp = new ZbusPlugin();
//创建一个MQ
zp.createMq("MyMQ");
//创建一个Topic（zbus中topic必须属于某个队列，所以第一个参数是mq名，第二个参数是topic名）
zp.createTopic("Topic", "Check");
//注册MQ的消息到达（收到消息）回调泛型（Dict类型，Dict继承子Model）接口。
zp.registMqMessageCallback("MyMQ", new TMessageCallback<Dict>(){
    @Override
    public void onMessage(Dict msg) {
        LOG.info("receive from MyMQ: key=" + msg.getStr("key") + ",value=" + msg.getStr("value"));
    }
});
//注册Topic的消息到达（收到消息）回调泛型（String类型）接口。
zp.registTopicMessageCallback("Topic", "Check", new TMessageCallback<String>(){
    @Override
    public void onMessage(String msg) {
        LOG.info("receive from Topic.Check: msg=" + msg);
    }
});
//添加插件
me.add(zp);
```

在其他地方，通过Zbus来发送消息。
```
//初始化一个MQ泛型（Dict类型，Dict继承子Model）发送器，构造函数参数为MQ名
Sender<Dict> mqSender = new MqSender<Dict>("MyMQ");
Dict dict = new Dict();
dict.setId(1L);
dict.set("key", "key"+1);
dict.set("value", "value"+1);
//发送对象到MQ
mqSender.send(dict);
//初始化一个Topic泛型（String类型）发送器，构造函数参数为，MQ名，Topic名
Sender<String> topicSender = new TopicSender<String>("Topic", "Check");
//发送对象到topic
topicSender.send("这时一个订阅消息");
```